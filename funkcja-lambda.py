import boto3
import csv

def lambda_handler(event, context):
    for record in event['Records']:
        bucket = 'laurasgh'
        file_key = 'test.csv'
        s3 = boto3.client('s3')
        csvfile = s3.get_object(Bucket=bucket, Key=file_key)
        csvcontent = csvfile['Body'].read().decode('utf-8').splitlines()
        lines = csv.reader(csvcontent)
        headers = next(lines)
        for line in lines:
            dynamodb = boto3.resource('dynamodb')
            table = dynamodb.Table('USLUGI')
            table.put_item(
            Item={
                    '_id': line[0], 'KONTRAKT_ID': line[1], 'USLUGA_ID': line[2], 'SEQNO': line[3], 
                    'DATA_ZMIANY_STATUSU': line[4], 'STATUS': line[5]
                })
